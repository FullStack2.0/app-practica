export interface UsersActualizacionResponse{
    name: string;
    job: string;
    updatedAt: Date
}