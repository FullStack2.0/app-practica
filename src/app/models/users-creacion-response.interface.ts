
export interface UsersCreacionResponse{
    name: string;
    job: string;
    id: string;
    createdAt: Date;
}