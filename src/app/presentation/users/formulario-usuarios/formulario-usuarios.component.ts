import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-formulario-usuarios',
  templateUrl: './formulario-usuarios.component.html',
  styleUrls: ['./formulario-usuarios.component.css']
})
export class FormularioUsuariosComponent implements OnInit {

  formUser!: FormGroup;

  constructor(
    private readonly router: Router,
    private readonly fb: FormBuilder
  ){}

  ngOnInit(): void {
    this.formUser = this.fb.group({
      name: [],
      job: []
    })
  }

  cancelar(){
    this.router.navigate(['/users'])
  }

}
