import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { UsersListadoResponse } from 'src/app/models/users-listado-response.interface';
import { UsersListado } from 'src/app/models/users-listado.interface';
import { UsersService } from 'src/app/services/users.service';

@Component({
  selector: 'app-listar-usuarios',
  templateUrl: './listar-usuarios.component.html',
  styleUrls: ['./listar-usuarios.component.css']
})
export class ListarUsuariosComponent implements OnInit, OnDestroy {

  displayedColumns: string[] = ['id', 'email', 'first_name', 'last_name', 'actions'];
  dataSource!: MatTableDataSource<UsersListado>;
  usersListSubs!: Subscription;

  constructor( 
    private readonly usersService: UsersService,
    private readonly router: Router
  ) { }
  
  ngOnInit(): void {
    this.listar();
  }

  listar(){
    this.usersListSubs = this.usersService.listar().subscribe(
      (res: UsersListadoResponse) => {
        this.dataSource = new MatTableDataSource<UsersListado>(res.data);
      }
    )
  }

  eliminar(id: number){
    if(!confirm(`¿Seguro que desea eliminar el usuario ${id}?`))
      return;

    this.usersService.eliminar(id).subscribe(
      res => {
        console.log('res: ', res);
        if(res.status === 204){
          alert('Eliminado Id =>'+id);
          this.listar();
        }
      },
      err => console.error(err),
      () => console.log('complete!')
    )
  }

  nuevo(){
    this.router.navigate(['/users/nuevo']);
  }

  ngOnDestroy(): void {
    this.usersListSubs.unsubscribe();
  }

}
