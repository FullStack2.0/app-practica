import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

import { LoginRequest } from 'src/app/models/login-request.interface';
import { LoginResponse } from 'src/app/models/login-response.interface';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, OnDestroy {
  
  formLogin!: FormGroup;
  authLoginSubs!: Subscription;

  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private router: Router
  ){}
    
  ngOnInit(): void {
    this.formLogin = this.fb.group({
      email: ['eve.holt@reqres.in'],
      password: ['cityslicka']
    })
  }

  ingresar(){
    const loginRequest: LoginRequest = {
      email: this.formLogin.controls['email'].value,
      password: this.formLogin.controls['password'].value
    }
    
    this.authLoginSubs = this.authService.login(loginRequest).subscribe(
      (res: LoginResponse) => {
        console.log('res: ', res);
        this.router.navigate(['/users']);
      },
      err => {
        console.error(err);
      },
      () => {
        console.log('complete!');
      }
    )
  }

  ngOnDestroy(): void {
    this.authLoginSubs.unsubscribe();
  }

}
