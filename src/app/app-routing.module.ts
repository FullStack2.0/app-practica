import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'auth/login', pathMatch: 'full' },
  {
    path: 'auth',
    loadChildren: () => import('../app/presentation/auth/auth.module').then(m => m.AuthModule)
  },
  { 
    path: 'users',
    loadChildren: () => import('../app/presentation/users/users.module').then(m => m.UsersModule)  
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
