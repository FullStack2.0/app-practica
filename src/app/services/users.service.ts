import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { UsersListadoResponse } from '../models/users-listado-response.interface';
import { UsersCreacion } from '../models/users-creacion.interface';
import { UsersCreacionResponse } from '../models/users-creacion-response.interface';
import { UsersActualizacionResponse } from '../models/users-actualizacion-response';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(
    private readonly http: HttpClient
  ) { }

  listar(): Observable<UsersListadoResponse>{
    return this.http.get<UsersListadoResponse>('https://reqres.in/api/users?page=1');
  }

  eliminar(id: number){
    return this.http.delete(`https://reqres.in/api/users/${id}`, { observe: 'response' });
  }

  insertar(user: UsersCreacion): Observable<UsersCreacionResponse>{
    return this.http.post<UsersCreacionResponse>(`https://reqres.in/api/users`,user);
  }

  actualizar(id: number, user: UsersCreacion): Observable<UsersActualizacionResponse>{
    return this.http.put<UsersActualizacionResponse>(`https://reqres.in/api/users/${id}`, user);
  }

}
